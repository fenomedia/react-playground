import React, { Component } from 'react';
import Persons from '../components/Persons/Persons';
import classes from './App.css';
import '../components/Persons/Person/Person.css'
import '../components/Cockpit/Cockpit'
import Cockpit from '../components/Cockpit/Cockpit';
import Aux from '../hoc/Aux'
import extendAux from '../hoc/extendAux';

export const AuthContext = React.createContext(false);

class App extends Component {

  state = {
    persons: [
      {id: 'asdfs', name: 'Martin', age: 30},
      {id: 'ewreer', name: 'André', age: 41},
      {id: 'bvnbv', name: 'Josi', age: 24}
    ],
    authenticated: false
  }



  constructor(props){
    super(props);
    console.log('[App.js] Inside Constructor', props);
  }

  componentDidMount(){
    console.log('[App.js] componentDidMount');

  }
  
  componentWillMount(){
    console.log('[App.js] Inside ComponentWillMount');
  
  }

  loginHandler = () => {
    this.setState({authenticated:true})
  }

  render() {
    console.log('[App.js] render');
    let persons = null;
    if(this.state.showPersons){
      persons = <Persons click={this.deletePersonHandler} change={this.nameChangedHandler} persons={this.state.persons}/>
    }

    return (
        <Aux>
          <AuthContext.Provider value={this.state.authenticated}>
            <button onClick={this.loginHandler}>Login</button>
            <Cockpit 
              title="Relevant Persons"
              showPersons={this.state.showPersons} 
              persons={persons} 
              togglePersonsHandler={this.togglePersonsHandler}/>
          </AuthContext.Provider>
        </Aux>
    );
  }
  

  

  nameChangedHandler = (event, id) => {
    const personIndex = this.state.persons.findIndex(p => {
      return p.id === id
    });
    const person = {...this.state.persons[personIndex]}; // Kopiert das Objekt (Immutable) Nicht direkt den State mutieren!
    person.name = event.target.value;
    const persons = [...this.state.persons];
    persons[personIndex] = person;

    this.setState({
      persons: persons
    })
  }

  deletePersonHandler = (index) => {
    //let persons = this.state.persons.slice(); // Kopiert das komplette Array
    let persons = [...this.state.persons] // Kopiert das komplette Array ES6
    persons.splice(index, 1)
    this.setState({
      persons: persons
    })
  }

  togglePersonsHandler = () => {
    this.setState((prevState, props) => {
      return {
        showPersons: !this.state.showPersons
      }
    })
  }

  
}

export default extendAux(App, classes.App);
