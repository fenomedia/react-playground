import React, { Component } from 'react';


const extendAux = (ComponentToWrap, className) => {
    const ExtendAux = class extends Component {
        render() {
            return (
                <div className={className}>
                    <ComponentToWrap ref={this.props.forwardRef} {...this.props} />
                </div>
            )
        }
    }

    return React.forwardRef((props, ref) => {
        return <ExtendAux {...props} forwardRef={ref} />
    });
}

export default extendAux;