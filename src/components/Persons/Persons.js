import React, {Component} from 'react';
import Person from './Person/Person'


class Persons extends Component {

    constructor(props){
        super(props);
        console.log('[Persons.js] Inside Constructor', props);
        this.personRef = React.createRef();
    }

    componentDidMount(){
        console.log('[Persons.js] componentDidMount');
        this.personRef.current.focus();
    }
    
    componentWillMount(){
        console.log('[Persons.js] Inside ComponentWillMount');
    }
    
    
    render() {
        console.log('[Persons.js] Inside render');
       return  <div>
            {
            this.props.persons.map((person, index) => {
                return  <Person 
                        ref={this.personRef}
                        key={person.id}
                        index={index}
                        click={() => {this.props.click(index)}}
                        changed={(event) => { this.props.change(event, person.id) }}
                        name={person.name} 
                        age={person.age}/>
            })
            }
        </div>
    }
}

export default Persons;


