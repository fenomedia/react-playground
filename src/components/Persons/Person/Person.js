import React, {Component} from 'react'
import classes from './Person.css'
import PropTypes from 'prop-types'
import extendAux from '../../../hoc/extendAux'
import {AuthContext} from '../../../containers/App';

class Person extends Component {
    constructor(props){
        super(props);
        console.log('[Person.js] Inside Constructor', props);
        this.inputField = React.createRef();
    }

    componentDidMount(){
        console.log('[Person.js] componentDidMount');
    }

    focus(){
        this.inputField.current.focus();
    }
    
    componentWillMount(){
        console.log('[Person.js] Inside ComponentWillMount');
    }
    
    
    render() {
        console.log('[Person.js] Inside render');
        return <div>
            <AuthContext.Consumer>
                {auth => auth ? <p>Authenticated User</p> : null }
            </AuthContext.Consumer>
            <p onClick={this.props.click}>I'm {this.props.name} and i am {this.props.age} years old.</p>
            <p><i>{this.props.children}</i></p>
            <p><input 
                    type="text" 
                    name="name"
                    value={this.props.name} 
                    onChange={this.props.changed}
                    ref={this.inputField}
                    /></p>
        </div>
    }
    
};

Person.propTypes = {
    click: PropTypes.func,
    age: PropTypes.number,
    changed: PropTypes.func,
    name: PropTypes.string
}

export default extendAux(Person, classes.Person);