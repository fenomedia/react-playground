import React from 'react';
import classes from './Cockpit.css';



const Cockpit = (props) => {
    let btnStyle = classes.Green;
    if(props.showPersons){
        btnStyle = classes.Red;
    }

    return (
        <div className={classes.Cockpit}> 
          <h1>{props.title}</h1>
          <button 
                className={btnStyle} 
                onClick={props.togglePersonsHandler}>Toggle Persons</button>
          {props.persons}
        </div>
    )
}

export default Cockpit;